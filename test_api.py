import pytest
import os
import requests

import api_handler

from api_exceptions import AuthorizationError, DataError

# def test_set_session_token_fail():
# 	"""If API return code not in (200, 401), return False"""

# 	tamigo_api_handler = api_handler.ApiHandler('1', '2')
# 	assert tamigo_api_handler.set_session_token() == False

# def test_set_session_token_fail_connection():
# 	"""If something is wrong with connection"""
# 	tamigo_api_handler = api_handler.ApiHandler(
# 		os.environ['TAMIGO_APP_NAME'],
# 		os.environ['TAMIGO_APP_KEY']
# 	)

# 	with pytest.raises((requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout)):
# 		tamigo_api_handler.set_session_token()

def test_set_session_token_fail_auth():
	"""If API return code is 401 (Unauthorized), raise exception"""
	tamigo_api_handler = api_handler.ApiHandler('1', '2')
	with pytest.raises(AuthorizationError):
		tamigo_api_handler.set_session_token()

def test_set_session_token_success():
	"""If API return code is 200 - everything is OK"""
	tamigo_api_handler = api_handler.ApiHandler(
		os.environ['TAMIGO_APP_NAME'],
		os.environ['TAMIGO_APP_KEY']
	)
	assert tamigo_api_handler.set_session_token() == True

def test_send_checkin_fail_auth():
    """If API return code is 401 (Unauthorized), raise exception"""
    tamigo_api_handler = api_handler.ApiHandler('1', '2')
    with pytest.raises(AuthorizationError):
        tamigo_api_handler.send_checkin(
            os.environ['TAMIGO_TEST_DEPARTMENT_KEY'],
            os.environ['TAMIGO_TEST_EMPLOYEE_KEY'],
            '2019-01-03T19:20:03'
            )

def test_send_checkin_fail_input_data():
    """If API return code is 200 but response is empty, raise exception"""
    tamigo_api_handler = api_handler.ApiHandler(
        os.environ['TAMIGO_APP_NAME'],
        os.environ['TAMIGO_APP_KEY']
    )
    with pytest.raises(DataError):
        tamigo_api_handler.send_checkin('wrong key', 'wrong key', '2019-01-03T19:20:03')

def test_send_checkin_success():
    """If API return code is 200 and response not empty - everything is OK"""
    tamigo_api_handler = api_handler.ApiHandler(
        os.environ['TAMIGO_APP_NAME'],
        os.environ['TAMIGO_APP_KEY']
    )
    assert tamigo_api_handler.send_checkin(
        os.environ['TAMIGO_TEST_DEPARTMENT_KEY'],
        os.environ['TAMIGO_TEST_EMPLOYEE_KEY'],
        '2019-01-03T19:20:03'
        ) == True

def test_send_checkout_fail_auth():
    """If API return code is 401 (Unauthorized), raise exception"""
    tamigo_api_handler = api_handler.ApiHandler('1', '2')
    with pytest.raises(AuthorizationError):
        tamigo_api_handler.send_checkout(
            os.environ['TAMIGO_TEST_DEPARTMENT_KEY'],
            os.environ['TAMIGO_TEST_EMPLOYEE_KEY'],
            '2019-01-03T19:20:03'
            )

def test_send_checkout_fail_input_data():
    """If API return code is 200 but response is empty, raise exception"""
    tamigo_api_handler = api_handler.ApiHandler('1', '2')
    with pytest.raises(AuthorizationError):
        tamigo_api_handler.send_checkout(
            os.environ['TAMIGO_TEST_DEPARTMENT_KEY'],
            os.environ['TAMIGO_TEST_EMPLOYEE_KEY'],
            '2019-01-03T19:20:03'
            )

