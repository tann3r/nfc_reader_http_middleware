import csv
import os

from imei_department_dict import ImeiDepartmentDict

def test_wrong_filename_correct_imei():
    """If wrong filename, return None"""
    csv_filename = 'temp_csv.csv'
    imei_fieldname = 'ImEi  '
    department_key_fieldname = 'department_ key'

    with open(csv_filename, 'w') as csv_file:
        writer = csv.DictWriter(csv_file, [imei_fieldname, department_key_fieldname])

        writer.writeheader()
        for i in range(9):
            writer.writerow({imei_fieldname: str(i) * 15, department_key_fieldname: i})

    os.environ['INOPTICS_IMEILIST_FILE'] = 'wrong_filename.csv"'
    id_dict = ImeiDepartmentDict()
    assert id_dict.get_department_key('1' * 15) == None

    os.remove(csv_filename)

def test_correct_filename_wrong_imei():
    """If correct filename, but wrong imei return None"""
    csv_filename = 'temp_csv.csv'
    imei_fieldname = 'ImEi  '
    department_key_fieldname = 'department_ key'

    with open(csv_filename, 'w') as csv_file:
        writer = csv.DictWriter(csv_file, [imei_fieldname, department_key_fieldname])

        writer.writeheader()
        for i in range(9):
            writer.writerow({imei_fieldname: str(i) * 15, department_key_fieldname: i})

    os.environ['INOPTICS_IMEILIST_FILE'] = csv_filename
    id_dict = ImeiDepartmentDict()
    assert id_dict.get_department_key('z' * 15) == None

    os.remove(csv_filename)

def test_correct_filename_correct_imei():
    """If correct filename and imei return department key"""
    csv_filename = 'temp_csv.csv'
    imei_fieldname = 'ImEi  '
    department_key_fieldname = 'department_ key'

    with open(csv_filename, 'w') as csv_file:
        writer = csv.DictWriter(csv_file, [imei_fieldname, department_key_fieldname])

        writer.writeheader()
        for i in range(9):
            writer.writerow({imei_fieldname: str(i) * 15, department_key_fieldname: i})

    os.environ['INOPTICS_IMEILIST_FILE'] = csv_filename
    id_dict = ImeiDepartmentDict()
    assert id_dict.get_department_key('1' * 15) == str(1)

    os.remove(csv_filename)

