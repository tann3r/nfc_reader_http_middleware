import pytz
import os
import xml.etree.ElementTree as et
from datetime import datetime, timezone, timedelta
from flask import Response

from utils import correct_device_time, get_timezone_for_tamigo

xml_msgs = {
    'stamping': '<?xml version="1.0" encoding="utf-8"?><inoptics><imei>111111</imei><msgid>1562</msgid><devtime>{}</devtime><tag><seconds_ago>10</seconds_ago><cardtype>20,MIFARE DESFIRE</cardtype><clocking><TransactionType>CR</TransactionType><DeviceModel>devmodel here</DeviceModel><TerminalID>111111</TerminalID><DateTime>2019-02-24T07:09:00</DateTime><TimeZone>180</TimeZone><UserID>111111</UserID><Direction>IN</Direction></clocking></tag></inoptics>',

    'alert': '<?xml version="1.0" encoding="utf-8"?><inoptics><imei>111111</imei><msgid>508</msgid><devtime>{}</devtime><seconds_ago>0</seconds_ago><alert><nick></nick><id>2</id><infotext>READER_DOWN:(timezone changed reset to(3))</infotext></alert></inoptics>',

    'status': '<?xml version="1.0" encoding="utf-8"?><inoptics><imei>111111</imei><msgid>512</msgid><devtime>{}</devtime><seconds_ago>1</seconds_ago><status></status></inoptics>'
}

def test_correct_time():
    """If time is correct (less than 20 seconds of difference, return None) """
    dt = datetime.now(timezone.utc).astimezone(pytz.timezone('Europe/Helsinki')).strftime('%Y-%m-%dT%H:%M:%S%z')
    dt = dt[:-2] + ':' + dt[len(dt)-2:]

    for type, msg in xml_msgs.items():
        root = et.fromstring(msg.format(dt))
        assert correct_device_time(root) == None


def test_respond_to_status_msg():
    """If time difference is more than 20 seconds, but less than 300,
    return flask Response only on status message"""
    dt = (datetime.now(timezone.utc).astimezone(pytz.timezone('Europe/Helsinki')) - timedelta(seconds=100)).strftime('%Y-%m-%dT%H:%M:%S%z')
    dt = dt[:-2] + ':' + dt[len(dt)-2:]

    for type, msg in xml_msgs.items():
        root = et.fromstring(msg.format(dt))

        if type == 'status':
            assert isinstance(correct_device_time(root), Response)
        else:
            assert correct_device_time(root) == None

def test_respond_to_status_and_stamping_msg():
    """If time difference is more than 300 seconds return flask Response on
    status and stamping messages"""
    dt = (datetime.now(timezone.utc).astimezone(pytz.timezone('Europe/Helsinki')) - timedelta(seconds=300)).strftime('%Y-%m-%dT%H:%M:%S%z')
    dt = dt[:-2] + ':' + dt[len(dt)-2:]

    for type, msg in xml_msgs.items():
        root = et.fromstring(msg.format(dt))

        if type in ['status', 'stamping']:
            assert isinstance(correct_device_time(root), Response)
        else:
            assert correct_device_time(root) == None

def test_timezone_env_isset():
    """If ITM_TIMEZONE environment var is set, return timezone"""
    os.environ['ITM_TIMEZONE'] = 'America/Indiana/Indianapolis'
    tz = pytz.timezone('America/Indiana/Indianapolis')

    assert tz == get_timezone_for_tamigo()

    del os.environ['ITM_TIMEZONE']

def test_timezone_env_wrong():
    """If ITM_TIMEZONE environment var is wrong, return Europe/Helsinki"""
    os.environ['ITM_TIMEZONE'] = 'Wonderland'
    tz = pytz.timezone('Europe/Helsinki')

    assert tz == get_timezone_for_tamigo()

    del os.environ['ITM_TIMEZONE']

def test_timezone_env_unset():
    """If ITM_TIMEZONE environment var is unset, return Europe/Helsinki"""
    tz = pytz.timezone('Europe/Helsinki')

    assert tz == get_timezone_for_tamigo()
