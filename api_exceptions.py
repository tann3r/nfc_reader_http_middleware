class CheckoutSettingError(Exception):
	"""Raised when there is no CheckIn record for this CheckOut to finish working shift"""
	pass

class AuthorizationError(Exception):
	"""Raised when response status code is 401"""
	pass

class DataError(Exception):
    """Raised when something is possibly wrong with input data (department/employee key)"""
