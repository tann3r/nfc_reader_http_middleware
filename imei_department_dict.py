import csv
import os
import logging

logger = logging.getLogger('flask.app')

class ImeiDepartmentDict():
    """Dictionary storage for IMEI/department key pairs. On init reads file declared in INOPTICS_IMEILIST_FILE environment variable"""
    def __init__(self):
        self.id_dict = {}
        try:
            with open(os.path.abspath(os.environ.get('INOPTICS_IMEILIST_FILE', None)), 'r') as csv_file:
                csv_reader = csv.reader(csv_file)
                imei_col_index = None
                department_key_col_index = None

                for row in csv_reader:
                    try:
                        if imei_col_index is None:
                            imei_col_index = [row.index(x) for x in row if 'imei' in x.strip().lower()][0]
                        if  department_key_col_index is None:
                            department_key_col_index = [row.index(x) for x in row if 'department' in x.strip().lower()][0]
                    except (ValueError, IndexError):
                        pass

                    if imei_col_index is not None and department_key_col_index is not None:
                        try:
                            self.id_dict[row[imei_col_index]] = row[department_key_col_index]
                        except IndexError as e:
                            logger.error('Something\'s wrong with csv file format, possibly missed or extra delimiter: {}'.format(e))

        except (TypeError, FileNotFoundError) as e:
            logger.error('Couldn\'t open file: {}'.format(str(e)))

        logger.info('Loaded imei/department dict: {}'.format(self.id_dict))


    def get_department_key(self, imei):
        """Returns department key corresponding to IMEI

        Args:
            imei: string, IMEI number

        Returns:
            department key string or None if no matches found"""
        if str(imei) in self.id_dict.keys():
            return self.id_dict.get(str(imei), None)
        else:
            logger.error('Couldn\'t find IMEI: {}'.format(imei))

        return None
