import logging
import os
import xml.etree.ElementTree as et
import pytz
from flask import Flask, request
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta, timezone

import api_handler
from utils import correct_device_time, get_current_utc_ntp_datetime, get_timezone_for_tamigo
from imei_department_dict import ImeiDepartmentDict

app = Flask(__name__)

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

executor = ThreadPoolExecutor(2)
tamigo_api_handler = api_handler.ApiHandler(
    os.environ['TAMIGO_APP_NAME'],
    os.environ['TAMIGO_APP_KEY']
    )
id_dict = ImeiDepartmentDict()



@app.route('/', methods=['GET', 'POST'])
def index():
    """Main endpoint for all NFC readers, accepts XML requests via POST, expects request to be in 'message=<?xml.....>' format. else throws exception

    Args:
        None

    Returns:
        XML correction response to 'status' and 'tag' messages if NFC reader has wrong datetime or OK (200)
    """
    app.logger.info('Received: {}'.format(request.values))
    app.logger.info('Received: {}'.format(request.get_data()))
    app.logger.info('Received: {}'.format(request.headers))

    if request.method == 'POST':
        try:
            root = et.fromstring(request.form.get('message', ''))
            time_correction = correct_device_time(root)

            # detect whether it's checkin/checkout and process it
            for item in root.iter('tag'):
                direction = item.find('./clocking/Direction').text.strip()
                user_id = item.find('./clocking/UserID').text.strip()

                if time_correction:
                    current_dt = get_current_utc_ntp_datetime()\
                                    .replace(tzinfo=timezone.utc)\
                                    .astimezone(get_timezone_for_tamigo())
                    seconds_ago = int(item.find('seconds_ago').text.strip())
                    event_dt_str = (current_dt - timedelta(seconds=seconds_ago)).strftime('%Y-%m-%dT%H:%M:%S')

                else:
                    event_dt_str = item.find('./clocking/DateTime').text.strip()
                    event_dt = datetime.strptime(event_dt_str, '%Y-%m-%dT%H:%M:%S')
                    event_dt = pytz.timezone('Europe/Helsinki')\
                                    .localize(event_dt)\
                                    .astimezone(get_timezone_for_tamigo())
                    event_dt_str = event_dt.strftime('%Y-%m-%dT%H:%M:%S')

                app.logger.info('Using {} timezone.'.format(get_timezone_for_tamigo()))

                if 'IN' in direction:
                    executor.submit(
                        tamigo_api_handler.send_checkin,
                        id_dict.get_department_key(root.find('imei').text.strip()),
                        user_id,
                        event_dt_str
                        )

                elif 'OUT' in direction:
                    executor.submit(
                        tamigo_api_handler.send_checkout,
                        id_dict.get_department_key(root.find('imei').text.strip()),
                        user_id,
                        event_dt_str
                        )

            if time_correction:
                return time_correction

        except (et.ParseError, AttributeError, ValueError) as e:
            app.logger.error('Failed to parse xml: {}\n{}'.format(request.form.get('message'), str(e)))


    return 'OK'
