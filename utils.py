import ntplib
import logging
import requests
import socket
import pytz
import os
import xml.etree.ElementTree as et
from retry import retry
from datetime import datetime, timezone
from urllib.parse import unquote
from flask import Response


import api_exceptions

logger = logging.getLogger('flask.app')

def catch_exception_after_retries(f):
    """Catches exceptions that Retry module throws after exceeding number of retries, writes them to logger
    """
    def wrapper(*w, **kw):
        try:
            return f(*w, **kw)
        except (
            requests.exceptions.ConnectionError,
            requests.exceptions.ReadTimeout,
            api_exceptions.AuthorizationError,
            api_exceptions.CheckoutSettingError,
            api_exceptions.DataError,
            ntplib.NTPException,
            socket.gaierror
        ) as e:
            logger.error('Could not finish request: {}'.format(str(e)))
    return wrapper

@catch_exception_after_retries
@retry((
    ntplib.NTPException,
    socket.gaierror
    ), tries=5, delay=3, logger=logger)
def get_current_utc_ntp_datetime():
    """Gets current datetime from NTP server

    Args:
        None

    Returns:
        Datetime object
    """
    c = ntplib.NTPClient()
    response = c.request('fi.pool.ntp.org', version=3)

    return datetime.utcfromtimestamp(response.tx_time)

def correct_device_time(req_root):
    device_datetime_str = unquote(req_root.find('devtime').text.strip())
    device_datetime = datetime.strptime(
        device_datetime_str[:device_datetime_str.rfind(':')] + \
        device_datetime_str[device_datetime_str.rfind(':')+1:],
        '%Y-%m-%dT%H:%M:%S%z'
        )
    current_datetime = get_current_utc_ntp_datetime().replace(tzinfo=timezone.utc).astimezone(pytz.timezone('Europe/Helsinki'))
    diff_in_secs = abs((current_datetime - device_datetime).total_seconds())

    logger.info('Difference in seconds: {}'.format(diff_in_secs))

    if req_root.find('alert') is None:
        res_root = et.Element('inoptics')
        res_cmds = et.SubElement(res_root, 'commands')
        cmds = None

        # if timezone changed, we update only timezone according to manual (page 15)
        if device_datetime.strftime('%z') != current_datetime.strftime('%z'):
            cmds = [
                'K=CFG_DEVICE_SET_TIMEZONE, V={}'.format(int(current_datetime.strftime('%z')[1:3])),
                'ACTION_DO TIME="{}"'.format(device_datetime.strftime('%y/%m/%d,%H:%M:%S')),
                'K=CFG_DEVICE_ALLOW_NW_TIME_SET, V=0'
            ]

        # if difference between 20 and 300 secs, respond only to status msg
        elif (20 < diff_in_secs < 300) and req_root.find('status') is not None:
            cmds = [
                'ACTION_DO TIME="{}"'.format(current_datetime.strftime('%y/%m/%d,%H:%M:%S')),
                'K=CFG_DEVICE_ALLOW_NW_TIME_SET, V=0'
            ]

        # if difference is more than 300 secs, respond to status and tag msgs
        elif diff_in_secs > 300:
            cmds = [
                'ACTION_DO TIME="{}"'.format(current_datetime.strftime('%y/%m/%d,%H:%M:%S')),
                'K=CFG_DEVICE_ALLOW_NW_TIME_SET, V=0'
            ]

        if cmds is not None:
            logger.info('Responding with datetime update to: {}, device datetime: {}, current datetime: {}'\
                .format(
                    req_root.find('imei').text.strip(),
                    req_root.find('devtime').text.strip(),
                    current_datetime.strftime('%Y-%m-%dT%H:%M:%S%z')
                )
            )

            for cmd in cmds:
                res_cmd = et.SubElement(res_cmds, 'cmd')
                res_cmd.text = cmd

            xml_declaration = '<?xml version="1.0" encoding="utf-8"?>'

            return Response(
                    response=xml_declaration+et.tostring(res_root, encoding='unicode'),
                    status=200,
                    mimetype="application/xml"
                )

    return None

def get_timezone_for_tamigo():
    if os.getenv('ITM_TIMEZONE', None) is not None:
        try:
            return pytz.timezone(os.environ['ITM_TIMEZONE'].strip())

        except pytz.exceptions.UnknownTimeZoneError as e:
            logger.error('Unknown timezone: {}, fallback to Europe/Helsinki'\
                .format(os.getenv('ITM_TIMEZONE', None))
                )

    return pytz.timezone('Europe/Helsinki')

