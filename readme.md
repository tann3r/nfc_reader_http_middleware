### HTTP middleware server (NFC readers -> Tamigo)  
#
This server app is intended to transform and transfer incoming data from multiple Inoptics NFC-TimeClean devices to Tamigo management system. The app processes only check-in/check-out data and sends it to Tamigo via API.

Before starting the server (startup.txt contains startup command) `TAMIGO_APP_NAME`, `TAMIGO_APP_KEY` and `TAMIGO_DEPARTMENT_KEY` environment variables have to be set (the app handles only one predefined department key currently)

Make sure the devices are configured according to manual.